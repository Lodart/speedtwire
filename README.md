# SpeedTwire

This is a fork of [Twire](https://github.com/Perflyst/Twire) which is a fork of the [Pocket Plays for Twitch](https://github.com/SebastianRask/Pocket-Plays-for-Twitch) Android application.
I will try to add the possibility to see speedrunners streams (e.g. from [Speedrun.com](https://www.speedrun.com/streams))

[Download](https://gitlab.com/Lodart/speedtwire/-/raw/master/app/release/app-release.apk)


## Licensing

Twire is licensed under the [GNU v3 Public License.](https://github.com/Perflyst/Twire/blob/master/LICENSE)

As this is a fork under GNUv3 there were other developers involved.
Copyright notice for: [SebastianRask](https://github.com/SebastianRask), [alexzorin](https://github.com/alexzorin), [Perflyst](https://github.com/Perflyst)
