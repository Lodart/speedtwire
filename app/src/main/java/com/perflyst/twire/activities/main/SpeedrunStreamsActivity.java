package com.perflyst.twire.activities.main;

import android.util.Log;

import com.perflyst.twire.R;
import com.perflyst.twire.adapters.MainActivityAdapter;
import com.perflyst.twire.adapters.StreamsAdapter;
import com.perflyst.twire.model.StreamInfo;
import com.perflyst.twire.service.StreamsService;
import com.perflyst.twire.views.recyclerviews.AutoSpanRecyclerView;
import com.perflyst.twire.views.recyclerviews.auto_span_behaviours.AutoSpanBehaviour;
import com.perflyst.twire.views.recyclerviews.auto_span_behaviours.StreamAutoSpanBehaviour;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.util.List;

public class SpeedrunStreamsActivity extends LazyMainActivity<StreamInfo> {

    @Override
    protected int getActivityIconRes() {
        return R.drawable.ic_action_rocket;
    }

    @Override
    protected int getActivityTitleRes() {
        return R.string.speedrun_streams_activity_title;
    }

    @Override
    protected AutoSpanBehaviour constructSpanBehaviour() {
        return new StreamAutoSpanBehaviour();
    }

    @Override
    protected MainActivityAdapter constructAdapter(AutoSpanRecyclerView recyclerView) {
        return new StreamsAdapter(recyclerView, this);
    }

    @Override
    public void addToAdapter(List<StreamInfo> streamsToAdd) {
        mOnScrollListener.checkForNewElements(mRecyclerView);
        mAdapter.addList(streamsToAdd);
        Log.i(LOG_TAG, "Adding Speedrun Streams: " + streamsToAdd.size());
    }

    @Override
    public List<StreamInfo> getVisualElements() throws JSONException, MalformedURLException {
        List<StreamInfo> mResultList;
        mResultList = StreamsService.fetchSpeedrunnersLiveStreams(getBaseContext(), getCurrentOffset(), settings.isHasPB());
        return mResultList;
    }
}
