package com.perflyst.twire.misc;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


public class SpeedrunnersUsernames {
    public static Set<String> getSpeedrunDotComStreamerNames(int offset, boolean hasPb){
        Log.d("SPEEDRUN", "Offset for speedrun.com retrieval : " + offset);

        Set<String> result = new HashSet<>();
        Document doc;
        String url = "https://www.speedrun.com/ajax_streams.php?";
        url += (hasPb ? "haspb=on" : "haspb=off");


        try {
            doc = Jsoup.connect(url + "&start=" + offset).get();
            Elements els = doc.select("a");
            for (Element el : els) {
                if (el.attr("href").contains("twitch.tv")){
                    String link = el.attr("href");
                    link = link.substring(22);
                    result.add(link);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
